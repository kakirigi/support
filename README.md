# Support

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]
[![Total Downloads][ico-downloads]][link-downloads]

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what
PSRs you support to avoid any confusion with users and contributors.

## Install

Via Composer

``` bash
$ composer require kakirigi/support
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CONDUCT](CONDUCT.md) for details.

## Security

If you discover any security related issues, please email kakirigi@gmail.com instead of using the issue tracker.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/kakirigi/support.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/kakirigi/support/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/kakirigi/support.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/kakirigi/support.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/kakirigi/support.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/kakirigi/support
[link-travis]: https://travis-ci.org/kakirigi/support
[link-scrutinizer]: https://scrutinizer-ci.com/g/kakirigi/support/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/kakirigi/support
[link-downloads]: https://packagist.org/packages/kakirigi/support
[link-author]: https://github.com/kakirigi
[link-contributors]: ../../contributors
